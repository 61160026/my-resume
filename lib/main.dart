import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: Text(
                  'ชื่อ-สกุล : จิรภัทร วิภาตนาวิน',
                  style: TextStyle( fontSize: 22),
                ),
              ),
              Text(
                'ชื่อเล่น : เอ็ม  อายุ : 21 ปี  เพศ : ชาย',
                style: TextStyle( fontSize: 22),
              ),
              Container(
                padding: EdgeInsets.only(top: 20, bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.laptop_chromebook,
                      color: Colors.green[700],
                    ),
                    Text(
                      ' Computer Skill',
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      'img/java.png',
                      width: 60,
                      height: 70,
                    ),
                    Image.asset(
                      'img/html.png',
                      width: 90,
                      height: 75,
                    ),
                    Image.asset(
                      'img/mysql.png',
                      width: 80,
                      height: 80,
                    ),
                    Image.asset(
                      'img/python.png',
                      width: 80,
                      height: 80,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 2, bottom: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.account_circle,
                      color: Colors.green[700],
                    ),
                    Text(
                      ' Personal Data',
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                      children: [
                        Text(
                          'วันเกิด : 24 ตุลาคม 2542',
                          style: TextStyle(fontSize: 18),
                        ),
                        Text(
                          'สัญชาติ : ไทย  เชื่อชาติ : ไทย',
                          style: TextStyle(fontSize: 18),
                        ),
                        Text(
                          'Tel : 090-7319070',
                          style: TextStyle(fontSize: 18),
                        ),
                        Text(
                          'Email : 61160026@go.buu.ac.th',
                          style: TextStyle(fontSize: 18),
                        ),
                      ],
                    ))
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 15, bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.menu_book,
                      color: Colors.green[700],
                    ),
                    Text(
                      ' Education',
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                      children: [
                        Text(
                          'ปริญญาตรี : มหาวิทยาลัยบูรพา',
                          style: TextStyle(fontSize: 18),
                        ),
                        Text(
                          'มัธยมศึกษา : โรงเรียนสิงห์สมุทร',
                          style: TextStyle(fontSize: 18),
                        ),
                        Text(
                          'ประถมศึกษา : โรงเรียนนาวิกโยธินบูรณะ',
                          style: TextStyle(fontSize: 18),
                        ),
                      ],
                    ))
                  ],
                ),
              ),
            ],
          )),
        ],
      ),
    );
    return MaterialApp(
      title: 'Jirapat Wipatnawin',
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Jirapat Wipatnawin'),
            backgroundColor: Colors.brown,
          ),
          body: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: Text(
                      'MY RESUME',
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.brown[400]),
                    ),
                  ),
                ],
              ),
              Image.asset(
                'img/myself.jpg',
                width: 600,
                height: 350,
                fit: BoxFit.fitHeight,
              ),
              titleSection,
            ],
          )),
    );
  }
}
